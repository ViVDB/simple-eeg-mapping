import scipy.signal
import os.path
import io
import numpy as np
import math
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider
from matplotlib import colors


class _Parsed:

    """
    Class reading an eeg file with a definite format

    Should be used as a new object, each method returns a useful parameter.
    """

    def __init__(self, location, name):
        file=open(os.path.join(location,name),"r",encoding="ISO-8859-14")
        self._metadata=dict()
        # processing first line :
        line=file.readline()
        liste=line.split()
        # format : names date Notch xx sampling rate xxx
        self._sampling_rate=liste.pop()
        liste.pop()
        liste.pop()
        self._metadata["notch"]=liste.pop()
        liste.pop()
        self._metadata["date"]=liste.pop()
        self._metadata["firstname"]=liste.pop()
        self._metadata["lastname"]=" ".join(liste)
        file.readline()
        file.readline()
        line=file.readline()
        liste=line.split()
        liste.pop()
        liste.pop()
        self._scale=liste
        for i in range(0,5):
            file.readline()
        # main data
        # tip from https://stackoverflow.com/questions/7133885/
        # ...fastest-way-to-grow-a-numpy-numeric-array
        # for speed
        streams=[]
        line=file.readline()
        while line:
            liste=line.split()
            # remove 2 useless streams
            liste.pop()
            liste.pop()
            # remove first if it's a time
            if ":" in liste[0]:
                del liste[0]
            # remove G2-G2 stream
            del liste[8]
            if len(liste)!=16:
                raise ValueError('Bad file formatting')
            streams.append(liste)
            line=file.readline()
        file.close()
        self._streams=np.reshape(streams, \
                      newshape=(len(streams), 16)).astype(np.float)

    def get_metadata(self):
        return self._metadata

    def get_sampling(self):
        return self._sampling_rate

    def get_scale(self):
        return max(self._scale)

    def get_data_streams(self):
        self._format()
        return self._formatted_data

    def _format(self):
        """put data in the right format"""
        # first scale everything correctly
        m=self.get_scale()
        for i in range(0,len(self._scale)):
            s=self._scale[i]
            if s<m:
                self._streams[i,:]=self._streams[i,:]*(m/s)
                self._scale=m
        # Expected stream convention (for now)
        # streams :
        # F8-G2     0
        # T4-G2     1
        # T6-G2     2
        # Fp2-G2    3
        # F4-G2     4
        # C4-G2     5
        # P4-G2     6
        # O2-G2     7
        # F7-G2     8
        # T3-G2     9
        # T5-G2     10
        # Fp1-G2    11
        # F3-G2     12
        # C3-G2     13
        # P3-G2     14
        # O1-G2     15
        # Electrode differences:
        # between the electrode and the mean of their
        # direct neighbors fot now
        # map:
        #   nan Fp1 Fp2 nan
        #   F7  F3  F4  F8
        #   T3  C3  C4  T4
        #   T5  P3  P4  T6
        #   nan O1  O2  nan
        s=np.size(self._streams,axis=0)
        self._formatted_data=np.zeros([5,4,s])
        self._formatted_data[0,0,:]=np.nan
        self._formatted_data[4,0,:]=np.nan
        self._formatted_data[0,3,:]=np.nan
        self._formatted_data[4,3,:]=np.nan
        # Fp1
        self._formatted_data[0,1,:]=self._streams[:,11] \
                                    - (self._streams[:,12] \
                                    + self._streams[:,3]) /2
        # Fp2
        self._formatted_data[0,2,:]=self._streams[:,3] \
                                    - (self._streams[:,4] \
                                    + self._streams[:,11]) /2
        # F7
        self._formatted_data[1,0,:]=self._streams[:,8] \
                                    - (self._streams[:,12] \
                                    + self._streams[:,9]) /2
        # F3
        self._formatted_data[1,1,:]=self._streams[:,12] \
                                    - (self._streams[:,11] \
                                    + self._streams[:,8] \
                                    + self._streams[:,4]\
                                    + self._streams[:,13]) /4
        # F4
        self._formatted_data[1,2,:]=self._streams[:,4] \
                                    - (self._streams[:,3] \
                                    + self._streams[:,0] \
                                    + self._streams[:,12]\
                                    + self._streams[:,5]) /4
        # F8
        self._formatted_data[1,3,:]=self._streams[:,0] \
                                    - (self._streams[:,4] \
                                    + self._streams[:,1]) /2
        # T3
        self._formatted_data[2,0,:]=self._streams[:,9] \
                                    - (self._streams[:,8] \
                                    + self._streams[:,13] \
                                    + self._streams[:,10]) /3
        # C3
        self._formatted_data[2,1,:]=self._streams[:,13] \
                                    - (self._streams[:,12] \
                                    + self._streams[:,9] \
                                    + self._streams[:,5]\
                                    + self._streams[:,14]) /4
        # C4
        self._formatted_data[2,2,:]=  self._streams[:,5] \
                                    -(self._streams[:,4] \
                                    + self._streams[:,13] \
                                    + self._streams[:,1]\
                                    + self._streams[:,6]) /4
        # T4
        self._formatted_data[2,3,:]=  self._streams[:,1] \
                                    -(self._streams[:,0] \
                                    + self._streams[:,5] \
                                    + self._streams[:,2]) /3
        # T5
        self._formatted_data[3,0,:]=  self._streams[:,10] \
                                    -(self._streams[:,9] \
                                    + self._streams[:,14]) /2
        # P3
        self._formatted_data[3,1,:]=  self._streams[:,14] \
                                    -(self._streams[:,13] \
                                    + self._streams[:,10] \
                                    + self._streams[:,6]\
                                    + self._streams[:,15]) /4
        # P4
        self._formatted_data[3,2,:]=  self._streams[:,6] \
                                    -(self._streams[:,5] \
                                    + self._streams[:,14] \
                                    + self._streams[:,2]\
                                    + self._streams[:,7]) /4
        # T6
        self._formatted_data[3,3,:]=  self._streams[:,2] \
                                    -(self._streams[:,6] \
                                    + self._streams[:,1]) /2
        # O1
        self._formatted_data[4,1,:]=  self._streams[:,15] \
                                    -(self._streams[:,7] \
                                    + self._streams[:,14]) /2
        # T5
        self._formatted_data[4,2,:]=  self._streams[:,7] \
                                    -(self._streams[:,15] \
                                    + self._streams[:,6]) /2

class EEG:
    """Class handling electro-encephalogram objects"""


    _FREQUENCY_LIMITS=[0,30]
    _ALPHA=[8,12]
    _BETA=[10,25]
    _DELTA=[0.5,4]
    _THETA=[4,8]

    def __init__(self, name, location="./data/"):

        file=_Parsed(location, name)
        self.metadata=file.get_metadata()
        self._sampling_rate=file.get_sampling()
        self.scaling=file.get_scale()
        self.data=file.get_data_streams()

    def fourier_transform(self):
        if not(hasattr(self,"frequencies")):
            # self._localize()
            self.frequencies=np.empty(self.data.shape[0:2],dtype='object')
            for l in range(0,5):
                for c in range(0,4):
                    stream=self.data[l,c,:]
                    if not math.isnan(stream[0]):
                        # f is the frequency in Hz
                        # t is the time in s
                        # np.abs(Zxx) is the amplitude of the frequency
                        f,t,Zxx=scipy.signal.stft(stream, \
                                fs=float(self._sampling_rate))
                        self.frequencies[l,c]=dict()
                        self.frequencies[l,c]["f"]=f
                        self.frequencies[l,c]["t"]=t
                        self.frequencies[l,c]["Z"]=Zxx

    # def _localize(self):
        # would be useful if everything wasn't done in the reader, which
        # I am still not sure is a good idea.

    def _horizontal(fig,t,freq=-1,time=-1):
        ax = fig.axes[0]
        box=ax.get_position()
        ax.set_position([box.x0,box.y0,0.85*box.width,box.height])
        plt.plot([t[0],t[len(t)-1]],[EEG._DELTA[0],EEG._DELTA[0]],label='min')
        plt.plot([t[0],t[len(t)-1]],[EEG._DELTA[1],EEG._DELTA[1]],label='max δ')
        plt.plot([t[0],t[len(t)-1]],[EEG._THETA[1],EEG._THETA[1]],label='max θ')
        plt.plot([t[0],t[len(t)-1]],[EEG._ALPHA[1],EEG._ALPHA[1]],label='max α')
        plt.plot([t[0],t[len(t)-1]],[EEG._BETA[1],EEG._BETA[1]],label='max β')
        plt.legend(loc="upper left", bbox_to_anchor=(1.01,1))
        plt.xlabel('t [s]')
        plt.ylabel('f [Hz]')
        if freq!=-1:
            plt.ylim(freq)
        else :
            plt.ylim(EEG._FREQUENCY_LIMITS)
        if time!=-1:
            plt.xlim(time)

    def stream_image(self,stream,time=-1, freq=-1):
        # stream has 2 numbers
        self.fourier_transform()
        t=self.frequencies[stream]["t"]
        f=self.frequencies[stream]["f"]
        Z=self.frequencies[stream]["Z"]
        # make a nice colormap of this stream
        fig=plt.figure()
        plt.pcolormesh(t, f, np.abs(Z),shading='gouraud')
        EEG._horizontal(fig,t,freq=freq,time=time)

        # try with the integrated scipy.signal.spectrogram as well

        fig=plt.figure()
        f,t,Sxx=scipy.signal.spectrogram(self.data[stream], mode='magnitude', \
                                fs=float(self._sampling_rate))
        plt.pcolormesh(t, f, Sxx, shading='gouraud')
        EEG._horizontal(fig,t,freq=freq,time=time)
        plt.show()

    def skull_image(self, time=-1):
        self.fourier_transform()
        # make a film or something with a slider for a time-dependent
        # 2D colormap of the skull main frequencies
        fig, ax =plt.subplots() # subplots instead of figure 'cause it creates a
                           # single subplot and gives the axes and has nice
                           # methods
        plt.subplots_adjust(bottom=0.25)
        t=self.frequencies[0,1]["t"]
        interval=t[1]-t[0]
        if time==-1:
            t_init=t[0]
            t_final=t[len(t)-1]
        else:
            t_init=time[0]
            t_final=time[1]
        f=np.empty([5,4])
        for i in range(0,5):
            for j in range(0,4):
                try:
                    whereto=((self.frequencies[i,j]["f"]<30) *\
                                (self.frequencies[i,j]["f"]>0.5))
                    index=np.argmax( np.abs( whereto* \
                        self.frequencies[i,j]["Z"][:,int(t_init/interval)]))
                    f[4-i,j]=self.frequencies[i,j]["f"][index]
                except:
                    f[4-i,j]=np.nan
        l=plt.pcolormesh(range(0,5),range(0,6),f,\
                                vmin=self._FREQUENCY_LIMITS[0], \
                                vmax=self._FREQUENCY_LIMITS[1] )
        plt.colorbar()
        axfreq = plt.axes([0.25, 0.1, 0.65, 0.03])
        sfreq = Slider(axfreq, 'Freq', t_init, t_final, \
                    valinit=t_init, valstep=interval, orientation='horizontal')
        def update(val):
            time=sfreq.val
            for i in range(0,5):
                for j in range(0,4):
                    try:
                        whereto=((self.frequencies[i,j]["f"]<30) *\
                                    (self.frequencies[i,j]["f"]>0.5))
                        index=np.argmax( np.abs( whereto* \
                            self.frequencies[i,j]["Z"][:,int(time/interval)]))
                        f[4-i,j]=self.frequencies[i,j]["f"][index]
                    except:
                        f[4-i,j]=np.nan
            l.changed()
            fig.canvas.draw_idle()
        sfreq.on_changed(update)
        ax.text(0.1,4.1,'nose up')
        ax.text(1.4,4.3,'Fp1')
        ax.text(2.4,4.3,'Fp2')
        ax.text(1.4,0.3,'O1')
        ax.text(2.4,0.3,'O2')
        plt.show()

    def skull_single_freq(self,frequency,time=-1):
        self.fourier_transform()
        # make a film or something with a slider for a time-dependent
        # 2D colormap of the intensity of the given frequency

        # switch frequency → get numerical values

        fig, ax =plt.subplots() # subplots instead of figure 'cause it creates a
                           # single subplot and gives the axes and has nice
                           # methods
        plt.subplots_adjust(bottom=0.25)
        t=self.frequencies[0,1]["t"]
        interval=t[1]-t[0]
        if time==-1:
            t_init=t[0]
            t_final=t[len(t)-1]
        else:
            t_init=time[0]
            t_final=time[1]
        f=np.empty([5,4])
        print(self.frequencies[1,1]["Z"][:,int(t_init/interval)].shape)
        print(self.frequencies[1,1]["f"].shape)
        print(self.frequencies[1,1]["f"])
        for i in range(0,5):
            for j in range(0,4):
                try:

                    # adapt here
                    f[4-i,j]=max(np.abs( \
                        self.frequencies[i,j]["Z"][:,int(t_init/interval)]))

                except:
                    f[4-i,j]=np.nan
        l=plt.pcolormesh(range(0,5),range(0,6),f)
        axfreq = plt.axes([0.25, 0.1, 0.65, 0.03])
        sfreq = Slider(axfreq, 'Freq', t_init, t_final, \
                    valinit=t_init, valstep=interval, orientation='horizontal')
        def update(val):
            time=sfreq.val
            for i in range(0,5):
                for j in range(0,4):
                    try:

                        # adapt here
                        f[4-i,j]=max(np.abs( \
                            self.frequencies[i,j]["Z"][:,int(time/interval)]))

                    except:
                        f[4-i,j]=np.nan
            l.changed()
            fig.canvas.draw_idle()
        sfreq.on_changed(update)
        ax.text(0.1,4.1,'nose up')
        ax.text(1.4,4.3,'Fp1')
        ax.text(2.4,4.3,'Fp2')
        ax.text(1.4,0.3,'O1')
        ax.text(2.4,0.3,'O2')
        plt.show()
