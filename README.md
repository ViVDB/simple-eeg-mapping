# Simple EEG mapping

This is (will be when I'll be done) a simple tool to transform eeg data (as formatted in the [Example](./data/Example.txt) file) into a picture of the different frequencies that appear in the brain.

Done after a discussion with technicians from the CHU of Liège.

More details to come.
